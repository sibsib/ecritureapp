import { Component, ViewChild, ElementRef } from '@angular/core';
import { lorem } from 'faker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  @ViewChild('userText') userTextField: ElementRef;
  randomText = lorem.sentence();
  enteredText = '';

  onChangeInput(value: string) {
    this.enteredText = value;
  }

  onClickNewGame(): void {
    this.randomText = lorem.sentence();
    this.enteredText = '';
    this.userTextField.nativeElement.select();
  }

  getClassName(index: number): string {
    if (
      index < this.enteredText.length &&
      this.randomText[index] === this.enteredText[index]
    )
      return 'letter-correct';
    if (
      index < this.enteredText.length &&
      this.randomText[index] !== this.enteredText[index]
    )
      return 'letter-incorrect';
    return 'letter-default';
  }
}
